# How to create your own doc

1. Fork our [repo]()
2. Rename it as your own in the settings
3. Clone it
4. Install `mkdocs`, your desired theme and extentions (or just run `run_local.sh`)
5. Edit the `nav` section on `mkdocs.yml` file and the wiki source files at the `docs/` folder
6. Setup the gitlab ci to host your page

And you're set!
