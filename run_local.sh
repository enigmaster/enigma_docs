#!/bin/bash

install_dep() {
    
echo -n "$1..."
if pip -q show mkdocs; then
    echo "ok"
else
    echo "install!"
    pip install $1
fi
}


echo Create and activate virtual environment
if [ ! -d .venv ]; then
    python3 -m venv .venv
fi
source .venv/bin/activate

echo Install dependencies
install_dep mkdocs
install_dep mkdocs-material
install_dep pymdown-extensions

# Start server
mkdocs serve
